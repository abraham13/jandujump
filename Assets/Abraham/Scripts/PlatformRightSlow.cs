﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRightSlow : MonoBehaviour
{
    int contador;
    bool ritmo = true;
    private float posx;
    private float posz;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.tag = "Player";
    }
    void Awake()
    {
        contador = 0;
    }

    // Update is called once per frame
    void Update()
    {
        posx = transform.position.x;
        posz = transform.position.z;
        if (ritmo == true)
        {
            transform.Translate(0.025f, 0, 0);
            contador++;
            if (contador == 50)
            {
                ritmo = false;
            }
        }

        if (ritmo == false)
        {
            transform.Translate(-0.025f, 0, 0);
            contador--;
            if (contador == -50)
            {
                ritmo = true;
            }
        }
    }
}