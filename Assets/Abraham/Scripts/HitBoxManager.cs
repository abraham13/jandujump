﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxManager : MonoBehaviour
{
    public GameObject player;
    public GameObject self;
    int x = 0;
    int y = 0;
    int z = 0;
    bool izquierda = false;
    bool attack = false;
    int time = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            izquierda = false;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            izquierda = true;
        }
        
        if (Input.GetKeyDown("c") && player.GetComponent<ControlPlayer>().isGrounded == false)
        {
            if (izquierda == false)
            { 
                this.transform.position = new Vector3(player.GetComponent<Transform>().position.x + 1.5f, player.GetComponent<Transform>().position.y, player.GetComponent<Transform>().position.z);
                attack = true;
            }
            else if (izquierda == true)
            {
                this.transform.position = new Vector3(player.GetComponent<Transform>().position.x - 1.5f, player.GetComponent<Transform>().position.y, player.GetComponent<Transform>().position.z);
                attack = true;
            }
        }

        if (attack == true)
        {
            if (time > 10)
            {
                this.transform.position = new Vector3(100f, 100f, 100f);
                time = 0;
                attack = false;
            }
            else
            {
                time++;
            }
        }
    }
}
